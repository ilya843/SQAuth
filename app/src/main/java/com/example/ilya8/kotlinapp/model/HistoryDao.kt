package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.*

@Dao
interface HistoryDao {

    @Insert
    fun insertAll(vararg histories: History)

    @Delete
    fun delete(history: History)

    @Query("SELECT * FROM history")
    fun getAllHistories(): MutableList<History>

    @Query("SELECT * FROM history WHERE id = :id")
    fun getHistory(id: Int): History

    @Update
    fun update(history: History)
}