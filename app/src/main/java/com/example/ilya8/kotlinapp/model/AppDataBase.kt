package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.Room
import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import kotlin.collections.ArrayList

class AppDataBase private constructor(context: Context) {

    private var myRoomDataBase: MyRoomDataBase
    private var userDao: UserDao
    private var siteDao: SiteDao
    private var historyDao: HistoryDao
    private var mListUsers: MutableList<User> = ArrayList()
    private var mListSites: MutableList<Site> = ArrayList()
    private var mListHistory: MutableList<History> = ArrayList()

    init {
        myRoomDataBase = Room.databaseBuilder(context, MyRoomDataBase::class.java, "database").run {
            allowMainThreadQueries()
            build()
        }

        userDao = myRoomDataBase.getUserDao()
        siteDao = myRoomDataBase.getSiteDao()
        historyDao = myRoomDataBase.getHistoryDao()

        mListUsers = userDao.getAllUsers()
        mListHistory = historyDao.getAllHistories()
        mListSites = siteDao.getAllSites()

    }

    companion object {
        private var appDataBase: AppDataBase? = null
        fun get(context: Context): AppDataBase? {
            if (appDataBase == null) {
                appDataBase = AppDataBase(context)
            }
            return appDataBase
        }
    }

    fun getSites(): List<Site>? {
        return siteDao.getAllSites()
    }

    fun getSite(id: Int): Site? {
        return siteDao.getSite(id)
    }

    fun getHistoryList(): List<History> {
        val histList = historyDao.getAllHistories()
        val site = getSites()
        val users = getUsersList()
        for (hist in histList){
            if (site != null) {
                hist.address = site.find { it.id == hist.idSite }!!.address
            }
            if (users != null) {
                hist.login = users.find { it.id == hist.idUser }!!.login
            }
        }
        return histList
    }

    fun getUser(userId: Int): User? {
        val user = userDao.getUser(userId)
        return user
    }

    fun getUsersList(): List<User>? {
        return userDao.getAllUsers()
    }

    fun updateUser(user: User){
        userDao.update(user)
    }

    fun insertSite(site: Site): Long{
        return siteDao.insertAll(site)[0]
    }

    fun getDefaultUser(): User?{
        return userDao.getDefaultUser()
    }

    fun getCountUsers(): Int{
        return userDao.getAllUsers().size
    }

    fun insertUser(user: User): Long {
        try {
            return userDao.insertAll(user)[0]
        } catch (ex: SQLiteConstraintException){}
        return -1
    }

    fun clearDefault() {
        userDao.clearDefault()
    }

    fun findUser(login: String): User? {
        return userDao.findUser(login)
    }

    fun getSiteByAddress(address: String, idUser: Int): Site? {
        return siteDao.getSiteByAddress(address,idUser)
    }

    fun insertHistory(history: History) {
        historyDao.insertAll(history)
    }

    fun updateSite(site: Site) {
        siteDao.update(site)
    }
}