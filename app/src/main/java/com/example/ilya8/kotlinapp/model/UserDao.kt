package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.*
import io.reactivex.Flowable
import java.util.*

@Dao
interface UserDao {
    @Insert
    fun insertAll(vararg users: User): List<Long>

    @Delete
    fun delete(user: User)

    @Query("SELECT * FROM users")
    fun getAllUsers(): MutableList<User>

    @Query("SELECT * FROM users WHERE id = :id")
    fun getUser(id: Int): User

    @Query("SELECT * FROM users WHERE defaultUser = 1")
    fun getDefaultUser(): User?

    @Update
    fun update(user: User)

    @Query("UPDATE users SET defaultUser = 0")
    fun clearDefault()

    @Query("SELECT * FROM users WHERE login = :login")
    fun findUser(login: String): User?

}