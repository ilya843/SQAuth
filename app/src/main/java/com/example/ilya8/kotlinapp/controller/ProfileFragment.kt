package com.example.ilya8.kotlinapp.controller

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.ilya8.kotlinapp.R
import com.example.ilya8.kotlinapp.model.AppDataBase
import com.example.ilya8.kotlinapp.model.User
import java.util.*

class ProfileFragment() : Fragment() {

    companion object {
        private val ARG_USER_ID: String = "user_id"
        private val ARG_INSERT_OR_UPDATE = "insert_or_update"

        private val INSERT: String = "insert"
        private val UPDATE: String = "update"

        fun newInstance(userId: Int): ProfileFragment {
            val args = Bundle()
            args.putInt(ARG_USER_ID, userId)
            args.putString(ARG_INSERT_OR_UPDATE, UPDATE)

            val fragment = ProfileFragment()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(): ProfileFragment {
            val args = Bundle()
            args.putString(ARG_INSERT_OR_UPDATE, INSERT)

            val fragment = ProfileFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var user: User? = null
    private var mode: String? = null

    private lateinit var mNameEditText: EditText
    private lateinit var mSurnameEditText: EditText
    private lateinit var mMiddleNameEditText: EditText
    private lateinit var mLoginEditText: EditText
    private lateinit var mEmailEditText: EditText
    private lateinit var mSaveButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(arguments!!.containsKey(ARG_USER_ID)) {
            val userId: Int = arguments!!.getInt(ARG_USER_ID)
            user = AppDataBase.get(activity!!.baseContext)!!.getUser(userId)
        } else{
            user = User()
        }
        mode = arguments!!.getString(ARG_INSERT_OR_UPDATE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_profile, container, false)

        mNameEditText = v.findViewById(R.id.editTextName)
        mSurnameEditText = v.findViewById(R.id.editTextSurname)
        mMiddleNameEditText = v.findViewById(R.id.editTextMiddleName)
        mLoginEditText = v.findViewById(R.id.editTextLogin)
        mEmailEditText = v.findViewById(R.id.editTextEmail)


        mNameEditText.setText(user!!.name, TextView.BufferType.EDITABLE)
        mSurnameEditText.setText(user!!.surname, TextView.BufferType.EDITABLE)
        mMiddleNameEditText.setText(user!!.middleName, TextView.BufferType.EDITABLE)
        mLoginEditText.setText(user!!.login, TextView.BufferType.EDITABLE)
        mEmailEditText.setText(user!!.email, TextView.BufferType.EDITABLE)


        mSaveButton = v.findViewById(R.id.buttonSave)
        mSaveButton.setOnClickListener {
            user!!.name = mNameEditText.text.toString()
            user!!.surname = mSurnameEditText.text.toString()
            user!!.middleName = mMiddleNameEditText.text.toString()
            user!!.login = mLoginEditText.text.toString()
            user!!.email = mEmailEditText.text.toString()
            user!!.defaultUser = 0
            user!!.idPhone = UUID.randomUUID().toString()

            if(mode == INSERT){
                val count = AppDataBase.get(activity!!.applicationContext)!!.getCountUsers()
                if(count == 0) {
                    user!!.defaultUser = 1
                } else {
                    user!!.defaultUser = 0
                }
                AppDataBase.get(activity!!.applicationContext)!!.insertUser(user!!)
            } else {
                AppDataBase.get(activity!!.applicationContext)!!.updateUser(user!!)
            }
        }

        return v
    }
}
