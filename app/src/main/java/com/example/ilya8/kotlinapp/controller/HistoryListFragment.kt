package com.example.ilya8.kotlinapp.controller

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.ilya8.kotlinapp.R
import com.example.ilya8.kotlinapp.model.AppDataBase
import com.example.ilya8.kotlinapp.model.History
import java.text.DateFormat

class HistoryListFragment: Fragment() {

    private lateinit var mHistoryRecyclerView: RecyclerView
    private lateinit var mAdapter: HistoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_history_list, container, false)

        mHistoryRecyclerView = v.findViewById(R.id.history_recycler_view)
        mHistoryRecyclerView.layoutManager = LinearLayoutManager(activity)

        updateUI()

        return v
    }

    private fun updateUI() {
        val dataBase = AppDataBase.get(activity!!.baseContext)
        val historyList = dataBase!!.getHistoryList()
        mAdapter = HistoryAdapter(historyList)
        mHistoryRecyclerView.adapter = mAdapter
    }

    private class HistoryHolder(inflater: LayoutInflater, container: ViewGroup?): RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_history, container, false)){

        private lateinit var mHistory: History

        private val mActionTextView: TextView = itemView.findViewById(R.id.history_action)
        private val mAddressTextView: TextView = itemView.findViewById(R.id.history_address)
        private val mLoginTextView: TextView = itemView.findViewById(R.id.history_login)
        private val mDateTextView: TextView = itemView.findViewById(R.id.history_time)

        fun bind(history: History){
            mHistory = history

            mActionTextView.text = mHistory.action
            mAddressTextView.text = mHistory.address
            mLoginTextView.text = mHistory.login
            mDateTextView.text = DateFormat.getInstance().format(mHistory.time)
        }

    }

    private inner class HistoryAdapter(listHistory: List<History>): RecyclerView.Adapter<HistoryHolder>() {

        private val mListHistory: List<History> = listHistory

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
            val layoutInflater = LayoutInflater.from(activity)
            return HistoryHolder(layoutInflater, parent)
        }

        override fun getItemCount(): Int {
            return mListHistory.size
        }

        override fun onBindViewHolder(holder: HistoryHolder, position: Int) {
            val history = mListHistory[position]
            holder.bind(history)
        }
    }

}