package com.example.ilya8.kotlinapp.controller

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import com.example.ilya8.kotlinapp.MainActivity
import com.example.ilya8.kotlinapp.R
import com.example.ilya8.kotlinapp.model.AppDataBase
import com.example.ilya8.kotlinapp.model.User

class UserListFragment : Fragment() {

    private lateinit var mUserRecyclerView: RecyclerView
    private lateinit var mAdapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_user_list, container, false)

        mUserRecyclerView = v.findViewById(R.id.user_recycler_view)
        mUserRecyclerView.layoutManager = LinearLayoutManager(activity)

        updateUI()

        return v
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.fragment_user_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.new_profile -> {
                val fm = fragmentManager
                fm!!.beginTransaction().replace(R.id.fragment_container,ProfileFragment.newInstance()).commit()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun updateUI() {
        val users = AppDataBase.get(activity!!)!!.getUsersList()
        mAdapter = UserAdapter(users as MutableList<User>)
        mUserRecyclerView.adapter = mAdapter
    }

    private inner class UserHolder(inflater: LayoutInflater, container: ViewGroup?) : RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_user, container, false)) {
        val mLoginTextView: TextView = itemView.findViewById(R.id.loginTextView)
        val mEmailTextView: TextView = itemView.findViewById(R.id.email_text_view_user)
        val mNameTextView: TextView = itemView.findViewById(R.id.name_text_view_user)
        val mSurnameTextView: TextView = itemView.findViewById(R.id.surname_text_view_user)
        val mMiddleNameTextView: TextView = itemView.findViewById(R.id.middle_name_text_view_user)
        val mIdPhoneTextView: TextView = itemView.findViewById(R.id.id_phone_text_view_user)
        val mRootContainer: ConstraintLayout = itemView.findViewById(R.id.id_user_list_item_constraint)
        val mDefaultSwitch: Switch = itemView.findViewById(R.id.switch_default)

        fun bind(user: User) {
            mLoginTextView.text = user.login
            mEmailTextView.text = user.email
            mNameTextView.text = user.name
            mSurnameTextView.text = user.surname
            mMiddleNameTextView.text = user.middleName
            mIdPhoneTextView.text = user.idPhone
            mDefaultSwitch.setChecked(user.defaultUser == 1)

            mRootContainer.setOnClickListener({
                if (mNameTextView.visibility == View.GONE) {
                    mNameTextView.visibility = View.VISIBLE
                    mSurnameTextView.visibility = View.VISIBLE
                    mMiddleNameTextView.visibility = View.VISIBLE
                    mIdPhoneTextView.visibility = View.VISIBLE
                } else {
                    mNameTextView.visibility = View.GONE
                    mSurnameTextView.visibility = View.GONE
                    mMiddleNameTextView.visibility = View.GONE
                    mIdPhoneTextView.visibility = View.GONE
                }
            })

            mDefaultSwitch.setOnCheckedChangeListener({ _, isChecked ->
                if (!isChecked) {
                    Toast.makeText(activity, "Всегда должен быть 1 профиль по-умолчанию", Toast.LENGTH_SHORT).show()
                    mDefaultSwitch.setChecked(!isChecked)
                    return@setOnCheckedChangeListener
                }
                user.defaultUser = 1
                val act: MainActivity = activity as MainActivity
                act.changeNavigationHeaderText(user.login)
                AppDataBase.get(activity!!)!!.clearDefault()
                AppDataBase.get(activity!!)!!.updateUser(user)
                updateUI()
            })
        }
    }

    private inner class UserAdapter(users: MutableList<User>) : RecyclerView.Adapter<UserHolder>() {
        val mUsers = users

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
            val lf = LayoutInflater.from(activity)
            return UserHolder(lf, parent)
        }

        override fun getItemCount(): Int {
            return mUsers.size
        }

        override fun onBindViewHolder(holder: UserHolder, position: Int) {
            val site = mUsers[position]
            holder.bind(site)
        }
    }

}