package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "history")
data class History(@PrimaryKey(autoGenerate = true)
                   var id: Int = 0,
                   @ForeignKey(entity = User::class,parentColumns = arrayOf("id"), childColumns = arrayOf("idUser"))
                   var idUser: Int = 0,
                   @ForeignKey(entity = Site::class,parentColumns = arrayOf("id"), childColumns = arrayOf("idSite"))
                   var idSite: Int = 0,
                   var address: String = "",
                   var action: String = "",
                   @Ignore
                   var login: String = "",
                   var time: Date = Date()) {
}