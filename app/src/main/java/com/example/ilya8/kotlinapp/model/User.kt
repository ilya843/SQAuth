package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import org.json.JSONObject
import java.util.*

@Entity(tableName = "users", indices = arrayOf(Index(value = ["login"], unique = true)))
data class User(@PrimaryKey(autoGenerate = true)
                var id: Int = 0,
                var login: String = "",
                var email: String = "",
                var name: String = "",
                var surname: String = "",
                var middleName: String = "",
                var idPhone: String = UUID.randomUUID().toString(),
                var defaultUser: Int = 0) {

    constructor(json: JSONObject) : this() {
        this.login = json.getString("login")
        this.email = json.getString("email")
        if(json.has("name")){
            this.name = json.getString("name")
        }
        if(json.has("surname")){
            this.surname = json.getString("surname")
        }
        if(json.has("middle_name")){
            this.middleName = json.getString("middle_name")
        }
    }

}