package com.example.ilya8.kotlinapp.model.support

import android.arch.persistence.room.TypeConverter
import java.text.DateFormat
import java.util.*

class DateConverter {
    @TypeConverter
    fun fromData(date: Date): String{
        return DateFormat.getInstance().format(date)
    }

    @TypeConverter
    fun toDate(dateString: String): Date{
        return DateFormat.getInstance().parse(dateString)
    }
}