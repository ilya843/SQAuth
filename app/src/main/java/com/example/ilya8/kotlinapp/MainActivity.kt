package com.example.ilya8.kotlinapp

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.TextView
import com.example.ilya8.kotlinapp.controller.*
import com.example.ilya8.kotlinapp.model.AppDataBase
import com.github.omadahealth.lollipin.lib.managers.AppLock
import com.github.omadahealth.lollipin.lib.managers.LockManager


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val REQUEST_FIRST_RUN_PIN: Int = 1
    private val REQUEST_CODE_ENABLE: Int = 2

    lateinit var mDrawerLayout: DrawerLayout
    lateinit var mToggle: ActionBarDrawerToggle

    val scanFragment: ScanPageFragment = ScanPageFragment()
    lateinit var profileFragment: ProfileFragment
    val sitesFragment: SiteListFragment = SiteListFragment()
    val historyFragment: HistoryListFragment = HistoryListFragment()
    lateinit var navView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)

        val lockManager =  LockManager.getInstance()
        lockManager.let {
            it.enableAppLock(this,MyPinActivity::class.java)
            it.appLock.timeout = 10000
            it.appLock.setShouldShowForgot(false)
        }


        if(!lockManager.appLock.isPasscodeSet){
            val intent = Intent(this,MyPinActivity::class.java)
            intent.putExtra(AppLock.EXTRA_TYPE,AppLock.ENABLE_PINLOCK)
            startActivityForResult(intent,REQUEST_FIRST_RUN_PIN)
        } else {
            val intent = Intent(this, MyPinActivity::class.java)
            intent.putExtra(AppLock.EXTRA_TYPE, AppLock.UNLOCK_PIN)
            startActivity(intent)
        }

        val fm: FragmentManager = supportFragmentManager
        var fragment: Fragment? = fm.findFragmentById(R.id.fragment_container)

        val count: Int = AppDataBase.get(this)!!.getCountUsers()

        if(count == 0){
            fragment = ProfileFragment.newInstance()
            fm.beginTransaction()
                    .add(R.id.fragment_container,fragment)
                    .commit()
        }else {
            if (fragment == null) {
                fragment = ScanPageFragment()
                fm.beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit()
            }
        }

        mDrawerLayout = findViewById(R.id.drawableLayout)
        mToggle = ActionBarDrawerToggle(this, mDrawerLayout,R.string.open,R.string.close)

        mDrawerLayout.addDrawerListener(mToggle)
        mToggle.syncState()

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        navView = findViewById(R.id.navigation_view)
        navView.setNavigationItemSelectedListener(this)

        val user = AppDataBase.get(this)!!.getDefaultUser()
        if (user != null) {
            changeNavigationHeaderText(user.login)
        }
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (mToggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val ftrans = supportFragmentManager.beginTransaction()

        when(id){
            R.id.nav_main_page -> ftrans.replace(R.id.fragment_container,scanFragment)
            R.id.nav_profile -> {
                val user = AppDataBase.get(this)!!.getDefaultUser()
                ftrans.replace(R.id.fragment_container, UserListFragment())
            }
            R.id.nav_change_pin -> {
                val intent = Intent(this, MyPinActivity::class.java)
                intent.putExtra(AppLock.EXTRA_TYPE, AppLock.CHANGE_PIN)
                startActivityForResult(intent, REQUEST_CODE_ENABLE)
            }
            R.id.nav_history -> ftrans.replace(R.id.fragment_container, historyFragment)
            R.id.nav_sites -> ftrans.replace(R.id.fragment_container,sitesFragment)
        }

        ftrans.commit()
        ftrans.addToBackStack(null);

        val drawerLayout: DrawerLayout = findViewById(R.id.drawableLayout)
        drawerLayout.closeDrawer(GravityCompat.START)

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        fragment.onActivityResult(requestCode, resultCode, data)
    }

    fun changeNavigationHeaderText(text: String){
        val header = navView.getHeaderView(0)
        val loginTextView: TextView = header.findViewById(R.id.loginTextView)
        loginTextView.text = text
    }

}
