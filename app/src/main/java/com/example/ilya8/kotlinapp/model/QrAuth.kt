package com.example.ilya8.kotlinapp.model

import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.util.Base64
import android.util.Log
import com.example.ilya8.kotlinapp.R
import org.json.JSONObject
import java.io.*
import java.lang.String.valueOf
import java.net.MalformedURLException
import java.net.URL
import java.security.*
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.sql.Timestamp
import java.util.*
import javax.net.ssl.*


class QrAuth private constructor(context: Context) {
    val PRIVATE_KEY_FILE: String = "privatekey.txt"
    val PUBLIC_KEY_FILE: String = "publickey.txt"


    val appDataBase = AppDataBase.get(context)
    var publicKey: PublicKey? = null
    var privateKey: PrivateKey? = null

    init {
        createCertificate(context)
    }

    companion object {
        var sslContext: SSLContext? = null
        private var mQrAuth: QrAuth? = null
        fun getInstance(context: Context): QrAuth {
            var qrAuth = mQrAuth;
            if (qrAuth == null) {
                qrAuth = QrAuth(context)
                sslContext = qrAuth.acceptCertificate(context)
            }
            return qrAuth
        }
    }

    fun action(qrMessage: String, user: User?) {
        var message = qrMessage
        message = message.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
        val parseMessage = message.split(":".toRegex(), 4).dropLastWhile { it.isEmpty() }.toTypedArray()

        val action = parseMessage[0]
        val randId = parseMessage[1]
        val address = parseMessage[2]
        val domainName = address.split("/")[0]

        val history = History()
        history.apply {
            idUser = if(user == null) -1 else user.id
            login = if(user == null) "nologin" else user.login
            this.address = domainName
        }

        when (action) {
            "r" -> {
                if (user != null) {
                    val idSite = appDataBase!!.insertSite(Site().apply {
                        idUser = user.id
                        login = user.login
                        this.address = domainName
                        dateLastVisit = Date()

                    })
                    history.action = "Быстрая регистрация"
                    history.idSite = idSite.toInt()
                    val builder = Uri.Builder()
                            .appendQueryParameter("fastRegistration", "")
                            .appendQueryParameter("login", user.login)
                            .appendQueryParameter("email", user.email)
                            .appendQueryParameter("phoneId", user.idPhone)
                            .appendQueryParameter("randId", randId)
                    NetworkOperation().execute("https://192.168.43.98/lib/QrAuth/PhoneRegister.php", builder.build().getEncodedQuery())
                }

            }
            "l" -> {
                //TODO добавить подпись полученного id сесси
                history.action = "Вход"
                if (user != null) {
                    val site: Site? = appDataBase!!.getSiteByAddress(domainName, user.id)
                    if(site == null){
                        return
                    }
                    site.dateLastVisit = Date()
                    appDataBase.updateSite(site)
                    history.idSite = site.id
                    val builder = Uri.Builder()
                            .appendQueryParameter("authUser", "")
                            .appendQueryParameter("phoneId", user.idPhone)
                            .appendQueryParameter("randId", randId)
                            .appendQueryParameter("time", valueOf(Timestamp(System.currentTimeMillis()).getTime()))
                    NetworkOperation().execute("https://192.168.43.98/lib/QrAuth/AccountAccess.php", builder.build().getEncodedQuery())
                }
            }
            "r_p" -> {
                history.action = "Привязка устройства"
                val json = JSONObject(parseMessage[3])
                val user = fillUser(json)
                val idSite = insertOrUpdateSite(user.id.toLong(),domainName)
//                val idSite = appDataBase.insertSite(Site().apply {
//                    this.idUser = idUser.toInt()
//                    login = user.login
//                    this.address = domainName
//                    dateLastVisit = Date()
//                })

                history.idUser = user.id
                history.login = user.login
                history.idSite = idSite!!.toInt()
                val builder = Uri.Builder().run {
                    appendQueryParameter("attachPhone", "")
                    appendQueryParameter("login", user.login)
                    appendQueryParameter("idPhone", user.idPhone)
                    appendQueryParameter("publicKey", getPublicKey())
                }
                NetworkOperation().execute("https://192.168.43.98/lib/QrAuth/PhoneRegister.php", builder.build().encodedQuery)
            }
        }
        appDataBase!!.insertHistory(history)
        //new NetworkOperation().execute("http://"+address,builder.build().getEncodedQuery());
        //                new NetworkOperation().execute(address,builder.build().getEncodedQuery());
    }

    private fun getPublicKey(): String {
        return Base64.encodeToString(publicKey!!.encoded, Base64.DEFAULT)
    }

    private fun fillUser(json: JSONObject): User {
        var user: User? = appDataBase!!.findUser(json.getString("login"))
        if (user == null) {
            user = User(json)
            if (appDataBase.getCountUsers() == 0) {
                user.defaultUser = 1
            }
            user.id = appDataBase.insertUser(user).toInt()
        }
        return user
    }

    private fun insertOrUpdateSite(idUser: Long, domainName: String): Long? {
        val site: Site? = appDataBase?.getSiteByAddress(domainName, idUser.toInt())
        val idSite: Long?
        if(site == null){
            idSite = appDataBase?.insertSite(Site().apply {
                this.idUser = idUser.toInt()
                address = domainName
                dateLastVisit = Date()
            })
        } else {
            idSite = site.id.toLong()
            site.dateLastVisit = Date()
            appDataBase!!.updateSite(site)
        }

        return idSite
    }

    private fun acceptCertificate(context: Context): SSLContext {
        val cf = CertificateFactory.getInstance("X.509")
        val caInput: InputStream = context.resources.openRawResource(R.raw.server)
        val ca: X509Certificate = caInput.use {
            cf.generateCertificate(it) as X509Certificate
        }

        val keyStoreType = KeyStore.getDefaultType();
        val keySrore = KeyStore.getInstance(keyStoreType).apply {
            load(null, null)
            setCertificateEntry("ca", ca)
        }

        val tmfAlgorithm: String = TrustManagerFactory.getDefaultAlgorithm();
        val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm).apply {
            init(keySrore)
        }

        val context2: SSLContext = SSLContext.getInstance("TLS").apply {
            init(null, tmf.trustManagers, null)
        }

        return context2
    }

    fun createCertificate(context: Context) {
        try {
            val privKeyFile = context.openFileInput(PRIVATE_KEY_FILE)
            val pubKeyFile = context.openFileInput(PUBLIC_KEY_FILE)

            val kf = KeyFactory.getInstance("RSA")

            val keySpec = PKCS8EncodedKeySpec(privKeyFile.readBytes())
            privateKey = kf.generatePrivate(keySpec)

            val x509keySpec = X509EncodedKeySpec(pubKeyFile.readBytes())
            publicKey = kf.generatePublic(x509keySpec)

        } catch (ex: FileNotFoundException) {
            val generator = KeyPairGenerator.getInstance("RSA")
            val keyPair = generator.genKeyPair()

            val publicKey = keyPair.public
            val privateKey = keyPair.private

            this.privateKey = privateKey
            this.publicKey = publicKey

            var file = context.openFileOutput(PRIVATE_KEY_FILE, Context.MODE_PRIVATE)
            file.write(privateKey.encoded)

            file = context.openFileOutput(PUBLIC_KEY_FILE, Context.MODE_PRIVATE)
            file.write(publicKey.encoded)
        }

    }

    private class NetworkOperation() : AsyncTask<String, Void, Void>() {
        override fun doInBackground(vararg strings: String): Void? {
            val address = strings[0]
            val params = strings[1]
            val url: URL
            try {
                val hostnameVerifier = HostnameVerifier { hostname, session ->
                    val hv = HttpsURLConnection.getDefaultHostnameVerifier()
                    //TODO Исправить этот момент (все сертификаты с неправильно заполненными полями будут приниматься)
                    hv.run { verify("localhost", session) }
                }
                //-----------------------------------

                url = URL(address)
                val conn = url.openConnection() as HttpsURLConnection
                conn.sslSocketFactory = sslContext?.socketFactory
                conn.hostnameVerifier = hostnameVerifier
                conn.setReadTimeout(10000)
                conn.setConnectTimeout(15000)
                conn.setRequestMethod("POST")
                conn.setDoInput(true)
                conn.setDoOutput(true)

                conn.setRequestProperty("Content-Length", "" + Integer.toString(params.toByteArray().size))

                val os = conn.getOutputStream()
                var data = params.toByteArray()
                os.write(data)
                conn.connect()

                val responseCode = conn.getResponseCode()

                Log.i("RESPONSE CODE", responseCode.toString())

                val istream = conn.getInputStream().bufferedReader().use { it.readText() }


            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: Throwable) {
                e.printStackTrace()
            }

            return null
        }
    }
}