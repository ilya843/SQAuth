package com.example.ilya8.kotlinapp.controller

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.ilya8.kotlinapp.CaptureActivityAnyOrientation
import com.example.ilya8.kotlinapp.MainActivity
import com.example.ilya8.kotlinapp.R
import com.example.ilya8.kotlinapp.model.AppDataBase
import com.example.ilya8.kotlinapp.model.QrAuth
import com.example.ilya8.kotlinapp.model.User
import com.google.zxing.integration.android.IntentIntegrator

class ScanPageFragment: Fragment() {

    lateinit var dataBase: AppDataBase
    var user: User? = null

    lateinit var scanContent: String
    lateinit var scanFormat: String
    lateinit var scanButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_scan_page,container,false)

        dataBase = AppDataBase.get(activity!!.applicationContext)!!
        user = dataBase.getDefaultUser()

        if(user == null){
            Toast.makeText(activity!!.applicationContext,"Отсутсвтует пользователь по-умолчанию", Toast.LENGTH_LONG).show()
        }

        scanButton = view.findViewById(R.id.buttonScan)
        scanButton.setOnClickListener({
            IntentIntegrator(activity).run {
                setPrompt("Scan")
                setBeepEnabled(true)

                setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)

                captureActivity = CaptureActivityAnyOrientation::class.java
                setOrientationLocked(true)
                setBarcodeImageEnabled(true)
                initiateScan()
            }
        })

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val qrAuth = QrAuth.getInstance(activity!!.applicationContext)
        val scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (scanningResult != null) {
            if (scanningResult.contents != null) {
                scanContent = scanningResult.contents
                scanFormat = scanningResult.formatName
            }

            //Toast.makeText(activity, "$scanContent   type:$scanFormat", Toast.LENGTH_SHORT).show()

            qrAuth.action(scanContent, user)

            if(user == null){
                user = dataBase.getDefaultUser()
            }

            val act: MainActivity = activity as MainActivity
            act.changeNavigationHeaderText(user!!.login)

        } else {
            Toast.makeText(activity, "Nothing scanned", Toast.LENGTH_SHORT).show()
        }
    }
}