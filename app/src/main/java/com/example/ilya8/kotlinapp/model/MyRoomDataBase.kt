package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.example.ilya8.kotlinapp.model.support.DateConverter

@Database(entities = arrayOf(User::class, Site::class, History::class), version = 1)
@TypeConverters(DateConverter::class)
abstract class MyRoomDataBase: RoomDatabase() {
    abstract fun getUserDao(): UserDao
    abstract fun getSiteDao(): SiteDao
    abstract fun getHistoryDao(): HistoryDao
}