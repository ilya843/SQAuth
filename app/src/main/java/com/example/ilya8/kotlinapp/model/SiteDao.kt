package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.*

@Dao
interface SiteDao {
    @Insert
    fun insertAll(vararg sites: Site): List<Long>

    @Delete
    fun delete(site: Site)

    @Query("SELECT * FROM sites")
    fun getAllSites(): MutableList<Site>

    @Query("SELECT * FROM sites WHERE id = :id")
    fun getSite(id: Int): Site

    @Update
    fun update(site: Site)

    @Query("SELECT * FROM sites WHERE address = :address AND idUser = :idUser")
    fun getSiteByAddress(address: String, idUser: Int): Site?
}