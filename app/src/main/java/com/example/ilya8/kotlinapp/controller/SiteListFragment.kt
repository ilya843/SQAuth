package com.example.ilya8.kotlinapp.controller

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.ilya8.kotlinapp.R
import com.example.ilya8.kotlinapp.model.Site
import com.example.ilya8.kotlinapp.model.AppDataBase
import java.text.DateFormat

class SiteListFragment: Fragment() {

    private var mSiteRecyclerView: RecyclerView? = null
    private var mAdapter: SiteAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_site_list, container, false)

        mSiteRecyclerView = view.findViewById(R.id.site_recycler_view)
        mSiteRecyclerView?.layoutManager = LinearLayoutManager(activity)

        updateUI()

        return view
    }

    private fun updateUI() {
        val dataBase = AppDataBase.get(activity!!.baseContext)
        val sites = dataBase!!.getSites()
        mAdapter = SiteAdapter(sites!!)
        mSiteRecyclerView!!.adapter = mAdapter
    }

    private inner class SiteHolder(inflater: LayoutInflater, container: ViewGroup?) : RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_site, container, false)) {

        private lateinit var mSite: Site

        private var mAddressTextView: TextView = itemView.findViewById(R.id.site_address)
        private var mLoginTextView: TextView = itemView.findViewById(R.id.user_login)
        private var mDateTextView: TextView = itemView.findViewById(R.id.last_enter)

        fun bind(site: Site){
            mSite = site
            mAddressTextView.text = mSite.address
            mLoginTextView.text = activity?.baseContext?.let { AppDataBase.get(it) }?.getUser(mSite.idUser)?.login ?: ""
            mDateTextView.text = DateFormat.getInstance().format(mSite.dateLastVisit)
        }

    }

    private inner class SiteAdapter(sites: List<Site>) : RecyclerView.Adapter<SiteHolder>() {

        private var mSites: List<Site> = sites

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SiteHolder {
            val layoutInflater = LayoutInflater.from(activity)
            return SiteHolder(layoutInflater, parent)
        }

        override fun getItemCount(): Int {
            return mSites.size
        }

        override fun onBindViewHolder(holder: SiteHolder, position: Int) {
            val site = mSites[position]
            holder.bind(site)
        }

    }

}