package com.example.ilya8.kotlinapp.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "sites")
data class Site(@PrimaryKey(autoGenerate = true)
                var id: Int = 0,
                @ForeignKey(entity = User::class, parentColumns = arrayOf("id"), childColumns = arrayOf("idUser"))
                var idUser: Int = 0,
                @Ignore
                var login: String = "",
                var address: String = "",
                var dateLastVisit: Date = Date()) {
}