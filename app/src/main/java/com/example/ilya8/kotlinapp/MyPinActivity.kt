package com.example.ilya8.kotlinapp

import android.graphics.Color
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity
import uk.me.lewisdeane.ldialogs.CustomDialog
import android.graphics.drawable.ColorDrawable
import com.github.omadahealth.lollipin.lib.managers.AppLock


class MyPinActivity: AppLockActivity() {
    override fun onPinSuccess(attempts: Int) {

    }

    override fun onPinFailure(attempts: Int) {

    }

    override fun showForgotDialog() {
    }

    override fun getStepText(reason: Int): String? {
        var msg: String? = null
        when (reason) {
            AppLock.DISABLE_PINLOCK -> msg = getString(R.string.pin_code_step_disabled_rus, this.pinLength)
            AppLock.ENABLE_PINLOCK -> msg = getString(R.string.pin_code_step_create_rus, this.pinLength)
            AppLock.CHANGE_PIN -> msg = getString(R.string.pin_code_step_change_rus, this.pinLength)
            AppLock.UNLOCK_PIN -> msg = getString(R.string.pin_code_step_unlock_rus, this.pinLength)
            AppLock.CONFIRM_PIN -> msg = getString(R.string.pin_code_step_enable_confirm_rus, this.pinLength)
        }
        return msg
    }

}